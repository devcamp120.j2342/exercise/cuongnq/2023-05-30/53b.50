import model.Account;
import model.Customer;

public class App {
    public static void main(String[] args) throws Exception {


        Customer customer1 = new Customer(1, "cuong", 200);
        Customer customer2 = new Customer(2, "phuong", 400);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        Account account1 = new Account(1, customer1, 123344.44);
        Account account2 = new Account(2, customer2, 22334.14);


        System.out.println(account1.toString());
        System.out.println(account2.toString());
    }
}
