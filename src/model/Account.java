package model;

public class Account {
    private int id;
    private Customer customer;
    private double balance;
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    @Override
    public String toString() {
        return "Account [id=" + id + ", customer=" + customer + ", balance=" + balance + "]";
    }


    public double deposit(double amount) {
        return this.balance += amount;
    }

    public double withdraw(double amount) {
        if (this.balance - amount >= 0) {
            return this.balance -= amount;
        }else {
            System.out.println("amount withdraw exceed the current balance");
        }
        return balance;
    }
    
    
}
